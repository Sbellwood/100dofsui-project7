//
//  ExpenseItem.swift
//  iExpense
//
//  Created by Skyler Bellwood on 7/9/20.
//  Copyright © 2020 Skyler Bellwood. All rights reserved.
//

import Foundation

struct ExpenseItem: Identifiable, Codable {
    let id = UUID()
    let name: String
    let type: String
    let amount: Int
}
